#!/usr/bin/env python3

from newspaper import Article
import datetime
import cgi
import json

from urllib.parse import urlparse
from newspaper.configuration import Configuration
from fnmatch import fnmatch

import sys
import base64
import os

import logging
logger = logging.getLogger(__name__)  
logger.setLevel(logging.DEBUG)

def getConfig(url):
    config = Configuration()
    host = urlparse(url).hostname
    if host is None:
        raise ValueError("url syntax error!")
    host = host.lower()

    if host.count('.') == 0 :
# or fnmatch(host, "plazza.orange.com"):
        return config
    else:
        try:
            os.environ['http_proxy']
            PROXIES = {
                'http': os.environ['http_proxy'],
                'https': os.environ['https_proxy']
            }
            config.proxies = PROXIES
        except KeyError:
            logger.error('No http_proxy defined')
        return config

def parseToJsonResult(article):
    try:
        article.parse()
    except Exception as e:
        msg = "Couldn't parse url " + article.url + " (cause: " + str(e) + ")"
        logger.error(msg);
        resp = Response(msg, status=500, content_type='application/json')
        return resp

    logger.debug("Newspaper3k: page parsed");
    article.nlp()

    logger.debug("Newspaper3k: preparing result");
    result = json.dumps({
		"url": article.url,
		"title": article.title,
		"text": article.text,
		"authors": article.authors,
		"publish_date": '' if article.publish_date is None else str(article.publish_date),
		"keywords": article.keywords,
		"summary": article.summary
	})
    logger.debug("Newspaper3k: returning result");
    return result

def downloadPage(url, page):
    article = Article(url)
    article.download(input_html=page)
    logger.debug("Newspaper3k: page downloaded");
    return article

def downloadUrl(url):
    if url is None:
        raise ValueError("url parameter is not defined!")

    configForProxy = getConfig(url)
    article = Article(url, config=configForProxy)
    article.download()
    return article

