#!/usr/bin/env python

from flask import Flask, request, Response
import logging

import json
import base64

import ParseWebPage


logging.basicConfig(level=logging.INFO)

# ------
# MAIN #
# ------

app = Flask(__name__)


@app.route('/parsewebpage',methods = ['POST', 'GET'])
def parseWebPage():
	try:
		url = request.form.get('url', None)
		logging.info("Flask received url: " + url);

		if 'pageAsFile' in request.files:
			file64 = request.files['pageAsFile']
			page64 = file64.read()
			page = base64.b64decode(page64)
			article = ParseWebPage.downloadPage(url, page)
		# elif "url" is not None:
		# 	article = ParseWebPage.downloadUrl(url)
		else:
			raise ValueError("Mandatory parameter 'pageAsFile' missing")

		result = ParseWebPage.parseToJsonResult(article);
		logging.info('Flask server: web page has been parsed and will be returned');
		return Response(result, status=200, content_type='application/json');		 

	except ValueError as error:
		resp = Response({json.dumps({"error": str(error)})}, status=400, content_type='application/json')
		logging.error(resp);
		return resp

	except Exception as e:
		if "failed with 404 Client Error" in str(e):
			msg = "download() failed with 404 Client Error";
			logging.error(msg);
			resp = Response({json.dumps({"error": str(msg)})}, status=404, content_type='application/json')
			return resp
		else:
			resp = Response({json.dumps({"error": str(e)})}, status=500, content_type='application/json')
			logging.error(resp);
			return resp


if __name__ == '__main__':
	app.run(host='0.0.0.0')

