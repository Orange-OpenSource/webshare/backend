/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package models

import com.typesafe.scalalogging.Logger

import scala.util._
import play.api.data._
import play.api.data.Forms._
import play.api.libs.json.{Json, Reads}
import play.api.mvc.MultipartFormData.FilePart
import play.api.libs.Files


/**
 * Type returned after the FORM mapping
 *
 * @param url the Page's url to be indexed (informative field, as no control is made it's the page's url),
 * @param quote parts of text quoted by the user in the Page displayed by this url,
 * @param tags list of tags to be applied on this web page
 *
 */
case class WebPageRequest(url: String, tags: String, quote: Option[String] = None)

/**
 * object used in controllers for Form parameters mapping
 */
object WebPageRequestForm {
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)

  /**
   * Check all json String Objects in the tagList can be converted as Tag objects
   */
  private def checkTagFromText(tagsArray: String): Boolean = {
    implicit val tagReader: Reads[Tag] = TagJson.TagReader

    Try(Json.parse(tagsArray).as[Seq[Tag]]) match {
      case Success(_) => true
      case Failure(err) =>
        logger.error("Parsing Tags error: " + err.getMessage)
        false
    }

//    tagList.map{ tag =>
//      Try(Json.parse(tag).as[Tag]) match {
//        case Success(_) => None
//        case Failure(err) =>
//          logger.error("Parsing Tags error: " + err.getMessage)
//          Some(err.getMessage)
//      }
//    }.forall(_.isEmpty)
  }

  /**
   * Form to object mapper
   */
  val userForm: Form[WebPageRequest] = Form(
    mapping(
      "url" -> text,
      "tags" -> text.verifying(tagsArray => checkTagFromText(tagsArray)),
      "quote" -> optional(text)
    )(WebPageRequest.apply)(WebPageRequest.unapply)
  )
}

/**
 * Wrapper for Web Page Indexing Request parameters
 */
case class WebPageRequestParams(url: String, pageAsPart: FilePart[Files.TemporaryFile], rawTags: String, quote: Option[String]) {
  implicit val tagReader: Reads[Tag] = TagJson.TagReader

  /**
   * Convert rawTags (as String Json) to Tags (objects)
   */
  val tags: Seq[Tag] = //rawTags.map{ tags =>
    Try(Json.parse(rawTags).as[Seq[Tag]]) match {
      case Success(t) => t
      case Failure(err) => throw new UnknownError("Validation should have been made before. Err: " + err.getMessage)
    }
//  }
}

object WebPageRequestHelper {
  /**
   * Utility used to have error message with page value shortened
   */
  def shortenPage(form: Form[WebPageRequest]): Map[String, String] = {
    val pageOpt = form.data.get("page")
    if (pageOpt.isDefined) 
      form.data updated ("page", pageOpt.get.take(10) + "...")
    else form.data
  }
}
