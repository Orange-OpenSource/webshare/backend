/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package models

/**
 * Allow to produced a report detailled by Tag, for operations performed for multiple Tags
 */

case class TagMessage(tagId: TagId, tag: Option[Tag], errMessage: String)

case class TagMessageList(liste: Seq[TagMessage], errorMsg: Option[String] = None) {
  def hasError: Boolean = errorMsg.nonEmpty || liste.map(_.tag).exists(_.isEmpty)
  def hasOnlyError: Boolean = !liste.map(_.tag).exists(_.isDefined)
  def errorsToString: String = {
    val msgsList = liste.map(_.errMessage).filter(_.nonEmpty)
    val prettyMsgsList = if (msgsList.isEmpty) "" else "\n\t* " + msgsList.mkString("\n\t* ")
    errorMsg.getOrElse("") + prettyMsgsList
  }
  def getErrorTagIds: Seq[TagId] = liste.filter(_.tag.isEmpty).map(_.tagId)
  def keepValids: TagMessageList = TagMessageList(liste.filter(_.tag.nonEmpty))
  def ++(listeToAdd: TagMessageList): TagMessageList = TagMessageList(liste ++ listeToAdd.liste)
}

object TagMessageList {
  def fromEither(eitherList: Iterable[Either[(TagId, String), Tag]]): TagMessageList = {
    val tagMessageIt =
      for (either <- eitherList)
      yield {
        either match {
          case Left((id, msg)) => TagMessage(id, None, msg)
          case Right(tag) => TagMessage(tag._id, Some(tag), "")
        }
      }
    val errMsgOpt = if (tagMessageIt.exists(_.errMessage.nonEmpty)) Some("Error Found in Tag List") else None
    TagMessageList(tagMessageIt.toSeq, errMsgOpt)
  }
  def empty: TagMessageList = TagMessageList(Nil)
  def withError(errMsg: String): TagMessageList = TagMessageList(Nil, Some(errMsg))
}



object TagMessageJson {

  import play.api.libs.json._
  import play.api.libs.functional.syntax._
  import TagJson.TagIdWriter
  import TagJson.TagWriter

  implicit val TagMessageWriter: Writes[TagMessage] = (
     (JsPath \ "requestedTagId").write[TagId] and
     (JsPath \ "tag").write[Option[Tag]] and
     (JsPath \ "message").write[String]
    )(unlift(TagMessage.unapply))

  implicit val TagMessageListWriter: Writes[TagMessageList] = (
    (JsPath \ "tagList").write[Seq[TagMessage]] and
    (JsPath \ "error").write[Option[String]]
    )(unlift(TagMessageList.unapply))


}