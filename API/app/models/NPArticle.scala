/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or  boomark pages for yourself,
 * and seek for information into all indexed pages using the search engine.
 */

package models

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class NPArticle (url: String, title: String, text: String, 
                      keywords: Option[Seq[String]], 
                      authors: Option[Seq[String]], 
                      publish_date: Option[String], 
                      summary: Option[String]) 

object NPArticleJson {
  
    implicit val NPArticleReader: Reads[NPArticle] = (
      (JsPath \ "url").read[String] and
      (JsPath \ "title").read[String] and
      (JsPath \ "text").read[String] and
      (JsPath \ "keywords").readNullable[Seq[String]] and
      (JsPath \ "authors").readNullable[Seq[String]] and
      (JsPath \ "publish_date").readNullable[String] and
      (JsPath \ "summary").readNullable[String]
  )(NPArticle.apply _)

  
  implicit val NPArticleWriter: Writes[NPArticle] = (
     (JsPath \ "url").write[String] and
     (JsPath \ "title").write[String] and
     (JsPath \ "text").write[String] and
     (JsPath \ "keywords").write[Option[Seq[String]]] and
     (JsPath \ "authors").write[Option[Seq[String]]] and
     (JsPath \ "publish_date").write[Option[String]] and
     (JsPath \ "summary").write[Option[String]]
    )(unlift(NPArticle.unapply))

}