/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package models

import java.text.SimpleDateFormat
import java.util.regex.Pattern

import com.typesafe.scalalogging.Logger
import models.WebPageJson.WebPageWriter
import play.api.libs.json._
import play.api.libs.functional.syntax._

case class WebPage(url: String, tagids: Seq[TagId], quote: Option[String],
                   title: String, text: String,
                   keywords: Option[Seq[String]], authors: Option[Seq[String]],
                   publish_date: Option[String], summary: Option[String]) {
  def toJsonShort: String = Json.prettyPrint(Json.toJson(this.copy(text = this.text.take(150) + " ....")))
}

object WebPage {
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)

  val dateRegex = "([0-9]{4}).([0-9]{2}).([0-9]{2})(.([0-9]{2}).([0-9]{2}).([0-9]{2})(.*?))?"
  val datePattern: Pattern = Pattern.compile(dateRegex)

  /**
   * Try to retrieve a Date from the dateRegex pattern in the 'strDate' parameter
   *
   * @param strDate the date to format as a 'reference' String Date
   * @return the String date if succedd
   */
  private def format(strDate: String): Option[String] = {
    val referenceDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val m = datePattern.matcher(strDate)
    if (m.matches()) {
      val heuresAsString =
        if (m.group(4) != null)  m.group(5) + ":" + m.group(6) + ":" + m.group(7)
        else "00:00:00"
      val dateAsString =
        m.group(1) + "-" + m.group(2) + "-" + m.group(3) + " " + heuresAsString
      logger.trace(s"Date to be parsed: $dateAsString")
      val date = referenceDateFormat.parse(dateAsString)
      Some(referenceDateFormat.format(date))
    } else None
  }

  def parseDate(strDateOpt: Option[String]): Option[String] = {
    if (strDateOpt.isDefined && strDateOpt.get.isEmpty)
      None
    else
      format(strDateOpt.get)
  }

  def apply(npArticle: NPArticle, tags: Seq[TagId], quote: Option[String]) =
    new WebPage(npArticle.url, tags, quote, npArticle.title, npArticle.text,
                npArticle.keywords,
                npArticle.authors,
                parseDate(npArticle.publish_date),
                npArticle.summary)

}

object WebPageJson {
  implicit val npArticleWriter: Writes[NPArticle] = NPArticleJson.NPArticleWriter
  implicit val tagIdWriter: Writes[TagId] = TagJson.TagIdWriter

  val strO: Option[String] = Some("")
  val str: Option[String] = strO.filter(_ != "")
  
  implicit val WebPageWriter: Writes[WebPage] = (
     (JsPath \ "url").write[String] and
     (JsPath \ "tagids").write[Seq[TagId]] and
     (JsPath \ "quote").writeNullable[String] and
     (JsPath \ "title").write[String] and
     (JsPath \ "text").write[String] and
     (JsPath \ "keywords").write[Option[Seq[String]]] and
     (JsPath \ "authors").write[Option[Seq[String]]] and
     (JsPath \ "publish_date").write[Option[String]] and
     (JsPath \ "summary").write[Option[String]]
    )(unlift(WebPage.unapply))


}
                  

                  