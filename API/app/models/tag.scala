/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package models

import java.nio.charset.StandardCharsets
import scala.util._
import com.typesafe.scalalogging.Logger


/**
 * A Tag is self defined by a 'Context/Name' unique String.
 * This unique String is the Tag 'raw' Id, and after base64 encoding becomes the Tag 'encoded' Id.
 * The 'raw' Tag Id is used for exchanges with the Client, and to refers to a Tag in a ES indexed Web Page.
 * The 'encoded' Tag Id is only used as ES _id
 *
 * @param label the Tag label
 * @param context the Tag Context
 */
class TagId(label: String, context: String) {
  if (context.isEmpty || label.isEmpty)
    throw new IllegalArgumentException(s"A tag must have a non empty label and context, found: '$context/$label'")
  val raw: String = context + "/" + label
  val encoded: String = java.util.Base64.getEncoder.encodeToString((context + "/" + label).getBytes(StandardCharsets.UTF_8))
}

object TagId {
  def apply(rawId: String): TagId = {
    if (!Tag.checkRawTagIds(rawId))
      throw new IllegalArgumentException(s"Malformed Tag Id: '$rawId'")
    val tagCtxtAndName = rawId.split('/')
    new TagId(tagCtxtAndName(1), tagCtxtAndName(0))
  }
  def decode(text: String): String = new String(java.util.Base64.getDecoder.decode(text))
}

/**
 * Tags are used to mark a Page with any information, as category, group of interest, ...
 * and will allow afterward to share documents with others based upon one or more tag(s),
 * or to facilitate searching indexed documents into ES.
 *
 * @param label the Tag label
 * @param context the Tag Context (may identifies a user's browser, an authenticated user, ..)
 * @param description an optional Tag description
 */
case class Tag(label: String, context: String, description: Option[String]) {
  val _id: TagId = new TagId(label, context)
}

/**
 * Tag Companion
 */
object Tag {
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)

  def fromESMap(fieldsAsMap: Map[String, Object]): Either[String, Tag] = {
    val cast = Try(
      (
        fieldsAsMap("label").asInstanceOf[String],
        fieldsAsMap("context").asInstanceOf[String],
        fieldsAsMap("description").asInstanceOf[String]
      )
    )
    cast match {
      case Success((label, context, description)) =>
        Right(Tag(label, context, Some(description)))
      case Failure(e) =>
        val errMsg = s"Couldn't cast tags item from ES to API. fieldsAsMap= $fieldsAsMap.\nerror msg= $e"
        logger.error(errMsg)
        Left(errMsg)
    }
  }

  /**
   *  Check all tags are formed like 'String/String' with none of the String empty
   */
  def checkRawTagIds(tagIds: String*): Boolean = {
    val split = tagIds.map(_.split('/'))
    !split.map(_.length).exists(v => v != 2) && !split.flatten.contains("")
  }

  /**
   * Convert a TagId to a Tag without description
   * @param rawTagIds the TagId list to convert
   */
  def tagIdsToTags(rawTagIds: Seq[TagId]): Seq[Tag] = {
    rawTagIds.map(_.raw.split('/')).map(strArr => Tag(strArr(1), strArr(0), None))
  }
}



object TagJson {

  import play.api.libs.json._
  import play.api.libs.functional.syntax._
  import play.api.libs.json.Reads._

  implicit val TagIdWriter: Writes[TagId] =
    Writes((id: TagId) => Json.toJson(id.raw))

  implicit val TagReader: Reads[Tag] = (
    (JsPath \ "label").read[String](minLength[String](1)) and
    (JsPath \ "context").read[String](minLength[String](2)) and
    (JsPath \ "description").readNullable[String]
    )(Tag.apply _)

  implicit val TagWriter: Writes[Tag] = (
    (JsPath \ "label").write[String] and
    (JsPath \ "context").write[String] and
    (JsPath \ "description").write[Option[String]]
    )(unlift(Tag.unapply))


}