/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package controllers

import play.api.mvc._
import play.api.inject.ApplicationLifecycle

import scala.concurrent.{ExecutionContext, Future}
import javax.inject._
import com.typesafe.scalalogging.Logger
import es.{ES, ESQueries, ESnapshot}
import commons.StringImprovements.StringImprovement
import org.elasticsearch.client.RestHighLevelClient

class ESController @Inject()(implicit cc: ControllerComponents, implicit val ec: ExecutionContext, val es: ES, val lifecycle: ApplicationLifecycle)
extends WSHController()
{
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)

  implicit val esClient: RestHighLevelClient = es.client

  /**
   * Elasticsearch connexion closer, triggered when this application stops
   */
  lifecycle.addStopHook { () =>
    logger.info("Closing ES Client connexion")
    Future.successful(esClient.close())
  }

  /**
   * Create the necessary ES Indices if they don't yet exist
   * Do nothing when they are
   */
  def getIndicesReady: Action[AnyContent] = Action.async {
    logger.info(reqHeader("/es/indices/getready"))
    ESQueries.doCreateIndicesIfNotExist() map {
      case (true, msg) =>
        logger.debug(msg)
        Ok(msg.toJson)
      case (false, msg) =>
        logger.error(msg)
        InternalServerError(msg.toJson)
    }
  }

  /**
   * In Progress...
   * Would make a ES backup (see application.conf for references)
   */
  def takeSnapshot: Action[AnyContent] = Action.async {
    logger.info(reqHeader("/es/indices/snapshot/take"))
    ESnapshot.doTakeSnapshot() map {
      case Right(msg) =>
        logger.debug(msg)
      Ok(msg.toJson)
      case Left(msg) =>
      logger.error(msg)
      InternalServerError(msg.toJson)
    }
  }

}