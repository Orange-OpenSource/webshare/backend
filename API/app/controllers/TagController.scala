/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package controllers

import play.api.libs.json._
import play.api.mvc._
import com.typesafe.scalalogging.Logger
import javax.inject._

import scala.concurrent.{ExecutionContext, Future}
import org.elasticsearch.client.RestHighLevelClient
import models.TagJson.{TagReader, TagWriter}
import models.TagMessageJson.TagMessageListWriter
import models._
import commons._
import es.{ES, ESQueries}


@Singleton
class TagController @Inject()(implicit cc: ControllerComponents, implicit val ec: ExecutionContext, val es: ES)
  extends WSHController()
{
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)

  implicit val esClient: RestHighLevelClient = es.client

  /**
   * INDEX
   */
  def indexTag: Action[JsValue] = Action(parse.json).async { implicit request =>
    logger.info(reqHeader(s"${request.uri} with attrs: ${request.body}"))

    readFromRequest[Tag] { tag =>
      ESQueries.doIndexTags(tag) map { returnedTagList =>
          val jsonTag = Json.toJson(returnedTagList)
          if (returnedTagList.hasError) {
            logger.error(Msg.logForClient(jsonTag))
            InternalServerError(jsonTag)
          } else {
            logger.debug(Msg.logForClient(jsonTag))
            Created(jsonTag)
          }
      }
    }
  }

  /**
   * SEARCH
   */
  def searchTags(text: String): Action[AnyContent] = Action.async { implicit request =>
    logger.info(reqHeader(s"${request.uri} with attrs: ${request.body}"))
    ESQueries.doSearchTag(text) map {
      case Right(tagList) => Ok(Json.toJson(tagList))
      case Left(errMsg) => InternalServerError(errMsg)
    }
  }
  

  /**
   * GET
   */
  def getTagsById: Action[JsValue] = Action(parse.json).async { implicit request =>
    logger.info(reqHeader(s"${request.uri} with attrs: ${request.body}"))

    readFromRequest[Seq[String]] { tagListId =>
      if (tagListId.isEmpty)
        Future { Ok(Json.toJson(TagMessageList(Nil))) }
      else if (!Tag.checkRawTagIds(tagListId: _*))
        Future { BadRequest(s"Malformed Tag(s) in Tag Id List: {${tagListId.mkString(", ")}}") }
      else {
        ESQueries.doGetTagsById(tagListId.map(TagId(_)): _*) map { tagMessageList =>
          if (tagMessageList.hasError) {
            val msg = tagMessageList.errorsToString
            logger.error(msg)

            if (tagMessageList.hasOnlyError)
              InternalServerError("No tag returned:\n" + msg)
            else
              Ok(Json.toJson(tagMessageList))

          } else {
            logger.debug(s"Tags with id ${tagListId.mkString(", ")} properly returned")
            Ok(Json.toJson(tagMessageList))
          }
        }
      }
    }
  }


  /**
   * UPDATE (Tag's description)
   */
  def updateTag(): Action[JsValue] = Action(parse.json).async { implicit request =>
    logger.info(reqHeader(s"${request.uri} with attrs: ${request.body}"))

    readFromRequest[Tag] { tag =>
      ESQueries.doUpdateTag(tag) map {
        case (true, msg) =>
          logger.debug(msg)
          NoContent
        case (false, msg) =>
          logger.error(msg)
          InternalServerError(msg)
      }
    }
  }


  
}