/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package controllers

import play.api.mvc._
import play.api.libs.ws._
import play.api.libs.json._
import play.api.libs.Files

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util._
import javax.inject._
import com.typesafe.scalalogging.Logger
import models.WebPageJson.WebPageWriter
import org.elasticsearch.client.RestHighLevelClient
import play.api.mvc.MultipartFormData.FilePart
//import org.apache.tika.metadata.Metadata
//import org.apache.tika.parser.{AutoDetectParser, ParseContext}
//import org.apache.tika.sax.BodyContentHandler
//import process.PdfParser
import models._
import newspaper.NPQueries
import es.{ES, ESQueries}
import commons.StringImprovements.StringImprovement
import models.NPArticleJson.NPArticleReader
import models.TagMessageJson.TagMessageListWriter
import models.WebPageJson.WebPageWriter


@Singleton
class UrlController @Inject()(implicit cc: ControllerComponents, ws: WSClient, implicit val ec: ExecutionContext, val es: ES)
  extends WSHController()
{
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)

  implicit val esClient: RestHighLevelClient = es.client

  /**
   * Local reader for the Form expected to index a Page
   */
  private def readFromWebPageIndexForm(f: WebPageRequestParams => Future[Result])
                                      (implicit request: Request[MultipartFormData[Files.TemporaryFile]], req: RequestHeader): Future[Result] =
  {
    val fileAsPartOpt = {
      request.body
        .file("pageAsFile")
        .map { pageAsFile =>
          logger.debug(s"Found pageAsFile (filename= '${pageAsFile.filename}') in request")
          pageAsFile
        }
    }
    if (fileAsPartOpt.isEmpty)
      Future { BadRequest(s"Malformed request: base64 file ('pageAsFile' param) not found \n") }
    else {
      WebPageRequestForm.userForm.bindFromRequest.fold(
        formWithErrors => {
          logger.error(s"Error reading incoming Form params: " + formWithErrors.errors.mkString(", "))

          Future { BadRequest(s"Malformed request: ${WebPageRequestHelper.shortenPage(formWithErrors)}\n") }
        },
        userData => {
          val url = userData.url
          val rawTags = userData.tags.trim
          val quote = userData.quote

          logger.debug(s"Forms params found in request: \n\turl: $url \n\trawTags: $rawTags  \n\tquote: $quote ")

          f(WebPageRequestParams(url, fileAsPartOpt.get, rawTags, quote))
        })
    }
  }



  /**
   * Index to ES, after parsing from base64 webpage file
   * If Tags sent with the Page don't exists, they'd be created
   */
  def indexPage: Action[MultipartFormData[Files.TemporaryFile]] = Action(parse.multipartFormData).async { implicit request =>
    logger.info(reqHeader(s"${request.uri} with attrs: \n\t${request.body.dataParts.mkString("\n\t")}"))

    readFromWebPageIndexForm { requestParams =>

      // Work in Progress...
      // preparing indexing Office doc & others also
      // val fileContent =
      // if (requestParams.pageAsPartOpt.isEmpty) ""
      //  else PdfParser.fileToString(requestParams.pageAsPartOpt.get)

      val tagIds = requestParams.tags.map(t => new TagId(t.label, t.context))
      logger.debug(s"About to run NewsPaper3K query for url: ${requestParams.url} and fileAsPart: ${requestParams.pageAsPart.filename}")
      NPQueries.doGetJsonContentFromPage(requestParams.url, requestParams.pageAsPart, ws) flatMap {
        case (true, json) =>
          ESQueries.getOrCreateTagsFut(requestParams.tags) flatMap { tagMsgList =>
            if (tagMsgList.hasError)
              Future { InternalServerError(s"Can't index page '${requestParams.url}' due to tag errors".toJsonErrorWith(Json.toJson(tagMsgList))) }
            else {
              val webPage = WebPage(json.as[NPArticle], tagIds, requestParams.quote)
              val webPageJson = Json.toJson(webPage)
              logger.trace(s"About to index webpage: \n${webPage.toJsonShort}")
              ESQueries.doIndexWebPage(webPage) map {
                case Right(_) =>
                  logger.info(s"Request Succeed: ${request.uri} with attrs: \n\t${request.body.dataParts.mkString("\n\t")}")
                  Created(webPageJson)
                case Left(jsonMsg) =>
                  logger.error(s"resp to Client: $jsonMsg")
                  InternalServerError(Json.prettyPrint(jsonMsg))
              }
            }
          }
        case (false, anyJson) =>
          logger.error("Newpaper3K Error:\n" + Json.prettyPrint(anyJson))
          Future { InternalServerError(anyJson) }
      }
    }
  }


    
} // class



