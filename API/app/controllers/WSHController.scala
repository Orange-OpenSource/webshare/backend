/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package controllers

import javax.inject._
import play.api.mvc._
import play.api.libs.json._
import play.api.http.{HttpEntity, Writeable}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import commons.StringImprovements.StringImprovement

@Singleton
class WSHController @Inject()(implicit ec: ExecutionContext, cc: ControllerComponents)
  extends AbstractController(cc)
{
  val higherLogger = com.typesafe.scalalogging.Logger(this.getClass)

  def options(path: String): Action[AnyContent] = Action {
    higherLogger.debug(s"OPTIONS $path")
    NoContent
  }

  /**
   * Common Reader for Json objects
   */
  def readFromRequest[T](f: T => Future[Result])(implicit request: Request[JsValue], rds: Reads[T], req: RequestHeader): Future[Result] = {
    request.body.validate[T].fold(
      errors => Future {
        val msgBase = for (error <- errors) yield "On param(s) '" + error._1.path.mkString(" ") + "': " + error._2.map(e => e.message).mkString(", ")
        val msg = "Malformed body: " + "\n\t" + msgBase.mkString("\n\t")
        higherLogger.error(msg)
        BadRequest(msg.toJsonError)
      },
      readValue => f(readValue)
    )
  }

  /**
   * Pretty print the Header log text
   * @param string the specific log String
   */
  def reqHeader(string: String): String = s"\n ${"-"*30}  NEW Request  ${"-"*30} \n $string \n ${"-"*75} "
}