/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package es

import play.api.libs.json._
import com.typesafe.scalalogging.Logger
import scala.util._
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import scala.collection.JavaConverters._
import org.elasticsearch.ElasticsearchException
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.indices.{CreateIndexRequest, GetIndexRequest}
import org.elasticsearch.action.{DocWriteRequest, DocWriteResponse}
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.update.UpdateRequest
import org.elasticsearch.action.search.{SearchRequest, SearchResponse}
import org.elasticsearch.action.get.MultiGetRequest
import org.elasticsearch.action.bulk.BulkRequest
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.common.unit.Fuzziness
import org.elasticsearch.index.query.MultiMatchQueryBuilder
import org.elasticsearch.rest.RestStatus
import org.elasticsearch.search.builder.SearchSourceBuilder

import resources.Config
import models.{TagMessage, _}
import models.TagJson.TagWriter
import models.WebPageJson.WebPageWriter
import commons.Msg
import commons.StringImprovements._


object ESQueries {
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)

  lazy val webPagesIndexName: String = Config.getString("services.elasticsearch.index.webpages")
  lazy val tagsIndexName: String = Config.getString("services.elasticsearch.index.tags")

  /**
   * Create the necessary ES Indices if they don't yet exist.
   * Do nothing when they are
   */
  def doCreateIndicesIfNotExist()(implicit esClient: RestHighLevelClient): Future[(Boolean, String)] = Future {
    val indicesNames = List(webPagesIndexName, tagsIndexName)
    val resultCreateIndex =
      for (indiceName <- indicesNames)
        yield {
          logger.debug(s"Check if index '$indiceName' exists")
          val existRequest = new GetIndexRequest(indiceName)
          val clientRequest = Try (esClient.indices().exists(existRequest, RequestOptions.DEFAULT)) match {
            case Success(value) => Right(value)
            case Failure(err) =>
              logger.error("\nES Client exception: " + Msg.getMyTrace(err))
              Left(("ES Client exception"))
          }

          if (clientRequest.isLeft)  (false, clientRequest.left.get)
          else {
            val exists = clientRequest.right.get
            if (exists) (true, s"Index '$indiceName' exists already, did nothing!")
            else {
              logger.debug(s"'$indiceName' doesn't exists. Getting mapping file '$indiceName.json'...")
              val mappingFile = Config.getMapping(indiceName)

              if (mappingFile.isEmpty) (false, s"Couldn't find the mapping file named '$indiceName'")
              else {
                logger.debug(s"Mapping file '$indiceName.json' found. Creating Index '$indiceName'..")
                val createIndexRequest = new CreateIndexRequest(indiceName)
                createIndexRequest.source(mappingFile.get, XContentType.JSON)
                val createIndexResponse = esClient.indices().create(createIndexRequest, RequestOptions.DEFAULT)
                val acknowledged = createIndexResponse.isAcknowledged
                if (acknowledged)
                  (true, s"Index '$indiceName' CREATED")
                else
                  (false, s"Index '$indiceName' couldn't be created")
              }
            }
          }
        }
    resultCreateIndex.reduce((acc, tuple) => (acc._1 && tuple._1, "\n\t" + acc._2 + "\n\t" + tuple._2 ))
  }

  /**************************
   *  Do Index Web Page     *
   **************************/
  def doIndexWebPage(webPage: WebPage)(implicit esClient: RestHighLevelClient): Future[Either[JsValue, Unit]] = Future {
    logger.debug(s"Prepare to index Web Page for url '${webPage.url}'")

    val webPageJson = Json.toJson(webPage)
    val indexRequest = new IndexRequest(webPagesIndexName)
    indexRequest.source(webPageJson.toString, XContentType.JSON)
    logger.trace(s"Start indexing webPage from ${webPage.url} to '$webPagesIndexName'")
    Try(esClient.index(indexRequest, RequestOptions.DEFAULT)) match {
      case Success(_) =>
        logger.debug(s"Web Page for url '${webPage.url}' indexed")
        Right(())
      case Failure(e: ElasticsearchException) =>
        logger.error(s"Couldn't index webPage in '$webPagesIndexName': \n\tES returned status: ${e.status()} \n\tES message: ${e.getDetailedMessage}")
        Left("Problem with elasticsearch. See logs for details".toJsonError)
      case Failure(e) =>
        logger.error(s"Impossible case! Couldn't index webPage in '$webPagesIndexName':  \n\tMessage: ${e.getMessage}")
        Left("Problem with elasticsearch. See logs for details".toJsonError)
    }
  }

  /**
   * Index one or more Tags in ES
   *
   * @param tags the one or multiple Tags to index
   */
  def indexTags(tags: Tag*)(implicit esClient: RestHighLevelClient): TagMessageList = {
    val tagInsertionStr = s"{${tags.map(_._id.raw).mkString(", ")}} to '$tagsIndexName'"
    logger.info(s"About to index tags $tagInsertionStr")
    val indexRequests =
      for (tagToInsert <- tags)
      yield {
        new IndexRequest(tagsIndexName)
          .opType(DocWriteRequest.OpType.CREATE)
          .id(tagToInsert._id.encoded)
          .source(Json.toJson(tagToInsert).toString, XContentType.JSON)
      }

    val bulkRequest = new BulkRequest
    indexRequests.map(bulkRequest.add)

    Try(esClient.bulk(bulkRequest, RequestOptions.DEFAULT)) match {
      case Success(bulkResponse) =>

        val errorTagMessages =
          bulkResponse.asScala.filter(_.isFailed).map { item =>
            logger.error(s"Error trying to index tag with Id '${item.getId}'")
            TagMessage(TagId(TagId.decode(item.getId)), None, item.getFailureMessage) }

        val okTagMessages =
          bulkResponse.asScala.filter(!_.isFailed).map { item =>
            logger.debug(s"Tag indexed ${TagId.decode(item.getId)}")
            TagMessage(TagId(TagId.decode(item.getId)), tags.find(_._id.encoded == item.getId), "")
          }

        TagMessageList((errorTagMessages ++ okTagMessages).toSeq)

      case Failure(err) =>
        logger.error("\nES Client exception: " + Msg.getMyTrace(err))
        TagMessageList.withError("ES Client exception")
    }
  }

  /**
   * Index one or more Tags in ES ASYNC
   */
  def doIndexTags(tag: Tag*)(implicit esClient: RestHighLevelClient): Future[TagMessageList] = Future {
    indexTags(tag: _*)
  }


  /**
   * Get or create tag(s) from an Id / seqence of Ids
   *
   * Found Tags are returned, and missing ones are created with empty description
   *
   * @param tags sequence of Tag
   * @return a TagMessageList with a general & detailled result
   */
  def getOrCreateTags(tags: Tag*)(implicit esClient: RestHighLevelClient): TagMessageList = {
    val tagIds = tags.map(t => new TagId(t.label, t.context))
    if (tagIds.isEmpty)
      TagMessageList.empty
    else {
      val tagsMessageList = getTagsFromIds(tagIds: _*)
      if (!tagsMessageList.hasError)
        tagsMessageList
      else {
        val missingTagIds = tagsMessageList.getErrorTagIds
        val resultTags =
          tagsMessageList.keepValids ++ indexTags(Tag.tagIdsToTags(missingTagIds): _*)

        if (resultTags.hasError)
          TagMessageList(resultTags.liste, Some("Some or all Tags couldn't be found neither created"))
        else
          resultTags
      }
    }
  }

  /**
   * Get or create tag(s) from an Id / seqence of Ids
   * @return a TagMessageList containing all tags (missing ones would be created with empty description)
   */
  def getOrCreateTagsFut(tagIds: Seq[Tag])(implicit esClient: RestHighLevelClient): Future[TagMessageList] =
    Future { getOrCreateTags(tagIds: _*) }


  /**
   * Get tag(s) from a seqence of Ids (/ a simple Id)
   * @return a TagMessageList containing the found tags and a error message for missing ones
   */
  def doGetTagsById(tagIds: TagId*)(implicit esClient: RestHighLevelClient): Future[TagMessageList] =
    Future { getTagsFromIds(tagIds: _*) }


  /**
   * Retrieve a list of tags (stored in Tag index) from their Id
   *
   * @param tagIds sequence of 'context/label'
   * @return a TagMessageList with a general & detailled result
   */
  def getTagsFromIds(tagIds: TagId*)(implicit esClient: RestHighLevelClient): TagMessageList = {
    val multiGetRequest = new MultiGetRequest()
    for (id <- tagIds)
      multiGetRequest.add(new MultiGetRequest.Item(tagsIndexName, id.encoded))

    val getResponseOrErr = Try(esClient.mget(multiGetRequest, RequestOptions.DEFAULT)) match {
      case Success(resp) => Right(resp)
      case Failure(err: ElasticsearchException) =>
        if (err.status() == RestStatus.NOT_FOUND)
          logger.error(s"Index '$tagsIndexName' not found!")
        else
          logger.error(s"Error while querying Index '$tagsIndexName': ${err.getDetailedMessage}")
        Left(s"Error while querying Index '$tagsIndexName'")
      case Failure(err) => Left(s"Error while querying Index '$tagsIndexName': ${err.getMessage}")
    }

    val resultIterableOrErr =
      if (getResponseOrErr.isLeft) Left(getResponseOrErr.left.get)
      else {
        Right {
          for {
            item <- getResponseOrErr.right.get.asScala
            tagId = TagId(TagId.decode(item.getId))
          } yield {
            if (item.isFailed) {
              val msg = s"ES Failure on tag id '${tagId.raw}': ${item.getFailure.getMessage}"
              logger.error(msg)
              Left(tagId, msg)
            } else { // Tag were found in ES
              val getResp = item.getResponse
              if (getResp.isExists) {
                Tag.fromESMap(getResp.getSourceAsMap.asScala.toMap) match {
                  case Right(tag) => Right(tag)
                  case Left(msg) => Left(tagId, msg)
                }
              } else {
                val msg = s"Tag id '${tagId.raw}' not found"
                logger.warn(msg)
                Left(tagId, msg)
              }
            }
          }
        }
      }

    if (resultIterableOrErr.isLeft)
      TagMessageList(Nil, Some(resultIterableOrErr.left.get))
    else
      TagMessageList.fromEither(resultIterableOrErr.right.get)
  }

  /**
   * Search a Tag by looking for 'words' into the Tags names, contexts and descriptions
   *
   * @param words the words we expect to find in tag's label, context or description
   * @return the list of found matching Tags
   */
  def doSearchTag(words: String)(implicit esClient: RestHighLevelClient): Future[Either[String, List[Tag]]] = Future {
    val searchRequest = new SearchRequest(tagsIndexName)
    val matchQueryBuilder = new MultiMatchQueryBuilder(words, "label", "context", "description")
    matchQueryBuilder.fuzziness(Fuzziness.AUTO)
    matchQueryBuilder.prefixLength(0)
    val searchSourceBuilder = new SearchSourceBuilder()
    searchSourceBuilder.query(matchQueryBuilder)
    searchRequest.source(searchSourceBuilder)


    Try(esClient.search(searchRequest, RequestOptions.DEFAULT)) match {
      case Success(searchResponse) =>
        val hits = searchResponse.getHits
        val searchHits = hits.getHits.toList
        val results =
          for (hit <- searchHits)
            yield {
              val tagAsMap = hit.getSourceAsMap.asScala + ("_id" -> hit.getId)
              Tag.fromESMap(tagAsMap.toMap)
            }
        Right(results.filter(_.isRight).map(_.right.get))
      case Failure(err) =>
        logger.error("\nES Client exception: " + Msg.getMyTrace(err))
        Left("ES Client exception")
    }
  }


  /**
   * Update Tag's description
   */
  def doUpdateTag(tag: Tag)(implicit esClient: RestHighLevelClient): Future[(Boolean, String)] = Future {
      logger.trace(s"Tag '${tag._id.raw}' will be update with description: ${tag.description.getOrElse("")}")

      val jsonMap = new java.util.HashMap[String, AnyRef]
      jsonMap.put("description", tag.description.getOrElse(""))
      val updateRequest = new UpdateRequest(tagsIndexName, tag._id.encoded).doc(jsonMap)

      Try(esClient.update(updateRequest, RequestOptions.DEFAULT)) match {
        case Success(updateResponse) =>
          updateResponse.getResult match {
            case DocWriteResponse.Result.UPDATED => (true, s"Tag with id '${tag._id.raw}' had its description updated")
            case DocWriteResponse.Result.NOOP  => (true, s"No Operation performed on tag (id '${tag._id.raw}')")
            case v => (false, s"WARN: Unexpected response ($v) from update for Tag with id '${tag._id.raw}'")
          }
        case Failure(e: ElasticsearchException) =>
          if (e.status() == RestStatus.NOT_FOUND) {
            val msg = s"Tag requested for update (id '${tag._id.raw}') doesn't exist: ${e.getMessage}"
            logger.error(msg)
            (false, msg)
          } else {
            val msg = s"Error while updating Tag of id '${tag._id.raw}': ${e.getMessage}"
            logger.error(msg)
            (false, msg)
          }
        case Failure(e) =>
          logger.error("Error in Tag updating: " + e.getMessage)
          throw e
       }
  }

  
  
}