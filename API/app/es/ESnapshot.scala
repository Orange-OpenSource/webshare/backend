/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package es

import com.typesafe.scalalogging.Logger
import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import scala.util._

import org.elasticsearch.ElasticsearchStatusException
import org.elasticsearch.action.admin.cluster.repositories.put.PutRepositoryRequest
import org.elasticsearch.action.admin.cluster.repositories.verify.VerifyRepositoryRequest
import org.elasticsearch.action.admin.cluster.snapshots.create.CreateSnapshotRequest
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.repositories.fs.FsRepository
import org.elasticsearch.rest.RestStatus

import resources.Config
import commons.Msg

/**
 * In Progress...
 *
 * Class responsible for ES backups & restores
 */
object ESnapshot {
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)

  lazy val snapshotLocation: String = Config.getString("services.elasticsearch.snapshot.location")
  lazy val snapshotRepoName: String = Config.getString("services.elasticsearch.snapshot.reponame")


  /**
   * Register Snapshot Repository
   *
   * @return true if the operation went well
   */
  private def registerSnapshotRepo()(implicit esClient: RestHighLevelClient): Boolean = {
    val locationKey = FsRepository.LOCATION_SETTING.getKey
    val compressKey = FsRepository.COMPRESS_SETTING.getKey
    val compressValue = true

    val settings = Settings.builder.put(locationKey, snapshotLocation).put(compressKey, compressValue).build
    val registerRequest = new PutRepositoryRequest
    registerRequest.settings(settings)
    registerRequest.name(snapshotRepoName)
    registerRequest.`type`(FsRepository.TYPE)

    Try(esClient.snapshot.createRepository(registerRequest, RequestOptions.DEFAULT)) match {
      case Success(response) =>
        response.isAcknowledged
      case Failure(err: java.io.IOException) =>
        logger.error(SNAPSHOT_IO_ERROR + "\n" + Msg.getMyTrace(err))
        false
      case Failure(err) =>
        logger.error(s"$SNAPSHOT_UNEXPECTED_ERROR. \n" + Msg.getMyTrace(err))
        false
    }
  }

  /**
   * createShapshot
   *
   */
  private def createSnapshot(snapshotName: String)(implicit esClient: RestHighLevelClient) = {
    val createShapshotRequest = new CreateSnapshotRequest
    createShapshotRequest.repository(snapshotRepoName)
    createShapshotRequest.snapshot(snapshotName)
    createShapshotRequest.waitForCompletion(true)

    Try {
      val response = esClient.snapshot.create(createShapshotRequest, RequestOptions.DEFAULT)
      val snapshotInfo = response.getSnapshotInfo
      val status = response.status

    } match {
      case Success(_) =>
      case Failure(err) =>
    }
  }

  final val SNAPSHOT_REPO_NOT_FOUND = "Snapshot Repository Not Found"
  final val SNAPSHOT_UNEXPECTED_ERROR = "Snapshot Unexpected Error"
  final val SNAPSHOT_IO_ERROR = "Couldn't access ES Snapshot"

  /**
   * Verifies that a snapshot repository is functional.
   */
  private def verifySnapshotRepo()(implicit esClient: RestHighLevelClient): Either[String, String] = {
    Try {
      val request = new VerifyRepositoryRequest(snapshotRepoName)
      val response = esClient.snapshot.verifyRepository(request, RequestOptions.DEFAULT)
      response.getNodes.asScala
    } match {
      case Success(repositoryMetaDataResponse) =>
        val nodeNames = for (resp <- repositoryMetaDataResponse) yield resp.getName
        Right(s"Snapshot repository verification OK. Nodes names: {${nodeNames.mkString(", ")}}")
      case Failure(err: ElasticsearchStatusException) =>
        if (err.status == RestStatus.NOT_FOUND) {
          logger.error(SNAPSHOT_REPO_NOT_FOUND + "\n" + Msg.getMyTrace(err))
          Left(SNAPSHOT_REPO_NOT_FOUND)
        } else {
          logger.error(s"$SNAPSHOT_UNEXPECTED_ERROR. Status=: ${err.status}\n" + Msg.getMyTrace(err))
          Left(SNAPSHOT_UNEXPECTED_ERROR)
        }
      case Failure(err: java.io.IOException) =>
        logger.error(SNAPSHOT_IO_ERROR + "\n" + Msg.getMyTrace(err))
        Left(SNAPSHOT_IO_ERROR)
      case Failure(err) =>
        logger.error(s"$SNAPSHOT_UNEXPECTED_ERROR. \n" + Msg.getMyTrace(err))
        Left(SNAPSHOT_UNEXPECTED_ERROR)
    }

  }

  /**
   *
   * @return
   */
  def doTakeSnapshot()(implicit esClient: RestHighLevelClient): Future[Either[String, String]] = Future {
    val checkRegistration = verifySnapshotRepo() match {
      case Left(SNAPSHOT_REPO_NOT_FOUND) =>
        if (registerSnapshotRepo())
          Left(s"Couldn't register Snapshot")
        else Right(())
      case Left(errMsg) => Left(s"Couldn't register Snapshot: $errMsg")
      case Right(_) => Right(())
    }

    if (checkRegistration.isLeft) Left(checkRegistration.left.get)
    else {
      logger.info("Registration OK - the remaining process is in progress..")
      Right("Registration OK - the remaining process is in progress..")
    }
  }
}
