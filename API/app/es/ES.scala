/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package es

import java.net.{URL, URLConnection}

import scala.util._
import com.typesafe.scalalogging.Logger
import org.apache.http.HttpHost
import org.apache.http.config.ConnectionConfig
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.elasticsearch.client.{RequestOptions, RestClient, RestClientBuilder, RestHighLevelClient}
import resources.Config

/**
 * ES object to be injected as dependency of controllers performing ES Requests
 */
class ES {
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)

  val host: String = Config.getString("services.elasticsearch.server.host")
  val port: Int = Config.getInt("services.elasticsearch.server.port1")

  import org.apache.http.HttpHost
  import org.apache.http.client.config.RequestConfig
  import org.elasticsearch.client.RestClient
  import org.elasticsearch.client.RestClientBuilder

  /**
   * Common ES Client for all requests
    */
  val client =  {

    val restClientBuilder =
      RestClient.builder(
        new HttpHost(host, port, "http"))

    val cli = new RestHighLevelClient(restClientBuilder)

//    logger.info(s"Try ES service...")
//    val info = cli.info(RequestOptions.DEFAULT)
//    logger.info(s"ES service ${info.getVersion} is started")

    cli
  }
}
