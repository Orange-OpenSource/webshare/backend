/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package newspaper

import com.typesafe.scalalogging.Logger
import play.api.libs.ws._
import play.api.libs.json._
import play.api.mvc.MultipartFormData.DataPart
import play.api.mvc.MultipartFormData._
import play.api.libs.Files

import scala.concurrent.{ExecutionContext, _}
import scala.util._
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.FileIO
import models.NPArticle
import models.NPArticleJson
import resources.Config
import commons._
import play.api.libs.json.JsError.toJson

/**
 * Interface with Newspaper3K Html parser
 */
object NPQueries {
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)
  implicit val npArticleReader: Reads[NPArticle] = NPArticleJson.NPArticleReader
  
  /**
   * doGetJsonContentFromParsing
   */
  def doGetJsonContentFromPage(url: String, pageAsPart: FilePart[Files.TemporaryFile], ws: WSClient)(implicit ec: ExecutionContext):
      Future[(Boolean, JsValue)] = 
 {
    val npUrl: String = Config.getString("services.newspaper.url")
    val pathToPart = java.nio.file.Paths.get(pageAsPart.ref.getAbsolutePath)

    logger.debug("Running Newspaper Request: " + npUrl)
    ws.url(npUrl).post(
        Source(
          DataPart("url", url) ::
          FilePart("pageAsFile", pageAsPart.filename, Option("text/html"), FileIO.fromPath(pathToPart)) ::
          List()
       )
     ) map { response =>
      logger.trace("Mapping response to npRequest...")
      val respJson = Try(response.json) match {
        case Success(json) => json
        case Failure(e) =>
          val msg = Msg.clearUnexpectedHtml(e.getMessage)
          logger.warn("npRequest response is not a Json, pb is: " + msg + "\ncause: " + e.getCause)
          if (response.body.contains("Article `download()` failed with HTTPSConnectionPool"))
            Json.toJson(s"Download failed for $url. Maybe a proxy pb, or a connexion refused by the server ?")
          else
            Json.toJson(response.body)
      }
      response.status match {
        case 200 =>
          logger.trace("npRequest returned 200")
          respJson.validate[NPArticle] match {
             case article: JsSuccess[NPArticle] =>
               logger.trace("json returned from Url parsing is valid")
               if (article.get.title == "Connexion" && article.get.text.startsWith("Error: You don't have JavaScript enabled.")) {
                 logger.error(s"Error in downloading the page at $url: Expect JavaScript enabled!")
                 (false, Msg.toJsonMessage(200, s"Error in downloading the page at $url", "Expect JavaScript enabled."))
               }
               else
                 (true, respJson)
             case errors: JsError =>
               logger.error("Couldn't parse Json returned from Html parser to an NPArticle: \n\tErrors: " + Json.prettyPrint(toJson(errors)) + "\n\tJson received: " + Msg.log(respJson))
               (false, Msg.toJsonMessage(200, "Error while querying an underlying service"))
          }
        case 500 =>
          logger.warn(s"Received From newspaper Http 500 (" + Msg.log(respJson) + ")")
          (false, Msg.toJsonMessage(500, "Error while querying an underlying service", respJson))
        case statusCode =>
          logger.error(s"Received From newspaper Http $statusCode (" + Msg.log(respJson) + ")")
          (false, Msg.toJsonMessage(statusCode, "Error while querying an underlying service", respJson))
      }
    }

 }

  
}