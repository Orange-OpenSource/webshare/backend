/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or  boomark pages for yourself,
 * and seek for information into all indexed pages using the search engine.
 */

package process

import java.io.File
import java.io.FileInputStream

import com.typesafe.scalalogging.Logger
import org.apache.tika.metadata.Metadata
import org.apache.tika.parser.{AutoDetectParser, ParseContext}
import org.apache.tika.parser.pdf.PDFParser
import org.apache.tika.sax.BodyContentHandler
import play.api.libs.Files
import play.api.mvc.MultipartFormData.FilePart

/**
 * In Progress...
 */
object PdfParser {
  val logger: Logger = com.typesafe.scalalogging.Logger(this.getClass)

  def fileToString(fileAsPart: FilePart[Files.TemporaryFile]): String = {
    val pathToPart = java.nio.file.Paths.get(fileAsPart.ref.getAbsolutePath)
    logger.debug(s"### pathToPart = '$pathToPart'")
    val inputstream = java.nio.file.Files.newInputStream(pathToPart)
    val parser = new AutoDetectParser
    val handler = new BodyContentHandler(10 * 1024 * 1024) // can read 10 million character documents
    parser.parse(inputstream, handler, new Metadata(), new ParseContext())
    handler.toString
  }

  def main(argv: Array[String]): Unit = {
    val handler = new BodyContentHandler()
    val metadata = new Metadata()
    val inputstream = new FileInputStream(new File("Example.pdf"))
    val pcontext = new ParseContext()

    //parsing the document using PDF parser
    val pdfparser = new PDFParser()
    pdfparser.parse(inputstream, handler, metadata,pcontext)

    //getting the content of the document
    println("Contents of the PDF :" + handler.toString)

    //getting metadata of the document
    println("Metadata of the PDF:")
    val metadataNames = metadata.names

    for (name <- metadataNames) {
      println(name + " : " + metadata.get(name))
    }
  }
}
