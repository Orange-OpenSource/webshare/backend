/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or  boomark pages for yourself,
 * and seek for information into all indexed pages using the search engine.
 */

package resources

import scala.util._
import java.io.File
import java.nio.file.Files
import com.typesafe.scalalogging.Logger


object Config {
  val logger: Logger = Logger(this.getClass)

  val configFile: com.typesafe.config.Config = com.typesafe.config.ConfigFactory.load()

  /**
   * Generic call to get a String in application.conf
   * @throws NoSuchElementException if 'prop' is not found in config file as a String
   */
  def getString(prop: String): String = {
    Try(configFile.getString(prop)) match {
      case Success(value) => value
      case Failure(f) =>
        throw new NoSuchElementException(s"Could not get String value in Config File for property '$prop': ${f.getMessage}")
    }    
  }

  /**
   * Generic call to get an Int in application.conf
   * @throws NoSuchElementException if 'prop' is not found in config file as an Int
   */
  def getInt(prop: String): Int = {
    Try(configFile.getInt(prop)) match {
      case Success(value) => value
      case Failure(f) =>
        throw new NoSuchElementException(s"Could not get Int value in Config File for property '$prop': ${f.getMessage}")
    }    
  }


  /**
   * Retrieve the mapping configuration (file in conf/es) for 'mappingName'
   * @return the file as String if found, or None
   */
  def getMapping(mappingName: String): Option[String] = {
    Try(new File(this.getClass.getClassLoader.getResource("es/" + mappingName + ".json").getFile)) match {
       case Success(file) => 
         val fileContent = new String(Files.readAllBytes(file.toPath))
         logger.trace("mapping file= " + fileContent)
         Some(fileContent)
       case Failure(err) =>
         logger.error(s"Didn't find mapping file $mappingName.json. \n\t Current path is " +
                        this.getClass.getClassLoader.getResource(".").getPath +
                        s"\nErr= ${err.getMessage}")
         None
    }
  }
  
}