/*
 * Software Name : Webshare
 * Version: 1.0.0
 * SPDX-FileCopyrightText: Copyright (c) 2020 - 2020 Orange
 * SPDX-License-Identifier: MIT
 *
 * This software is distributed under the MIT License,
 * the text of which is available at https://opensource.org/licenses/MIT
 * or see the "license.txt" file for more details.
 *
 * Author: Muriel Le Metayer, Julien Delaby, Florent Glauda
 * Software description:
 * Allow to easily share web pages with communities, or boomark pages for yourself,
 * and then lookup up into all indexed pages using the search engine.
 */

package commons

import play.api.libs.json._

/**
 * An helper object for formatting Messages
 */
object Msg {
  implicit val jsonMessageWriter = JSonMessage.JSonMessageWriter

  def toJsonMessage(status: Int, msg: String, details: JsValue) = Json.toJson(JSonMessage(status, msg, details))
  def toJsonMessage(status: Int, msg: String, details: String = "None") = Json.toJson(JSonMessage(status, msg, Json.toJson(details)))
  
  def logForClient(json: JsValue) = "Message returned to Client: " + Json.prettyPrint(json)
  def log(json: JsValue) = Json.prettyPrint(json)
  
  def clearUnexpectedHtml(msg: String) = {
    if (msg.contains("Unexpected character ('<' (code 60))") || 
        (msg.contains("<html>") && msg.contains("<body>"))) {
      val newMess = msg.split("\n").mkString(" ")
      "</body>.+".r.replaceFirstIn(".+<body>".r.replaceFirstIn(newMess, ""), "")
    }
    else msg
  }

  /**
   * Format an Exception to a trace usable in logging function that output only applications lines
   *
   * @param error the exception to format
   * @return a String
   */
  def getMyTrace(error: Throwable): String = {
    val packageName = this.getClass.getPackage.getName.takeWhile(_ != '.')
    val errMsg = "\tErr= " +
      (if (error.getMessage.contains(error.getClass.getCanonicalName)) ""
      else s"${error.getClass.getCanonicalName}: ") +
      s"${error.getMessage}"

    s" \n$errMsg" +
      "\n\tat " +
      error.getStackTrace.map(line => line.getClassName + "." + line.getMethodName + ":" + line.getLineNumber)
        .filter(p => p.startsWith(packageName)).mkString("\n\tat ") +
      (if (error.getCause != null) "\n\tCause= " + error.getCause else "") + "\n"
  }

}


case class JSonMessage(status: Int, message: String, details: JsValue)
object JSonMessage {

  implicit val JSonMessageWriter = new Writes[JSonMessage] {
    def writes(msg: JSonMessage) = Json.obj(
        "status" -> msg.status,
        "message" -> msg.message,
        "details" -> msg.details
    )   
  }
}


object StringImprovements {
  implicit class StringImprovement(msg: String) {
    def toJson: JsValue = Json.toJson(Json.obj("message" -> msg))
    def toJsonError: JsValue = Json.toJson(Json.obj("errorMessage" -> msg))
    def toJsonErrorWith(obj: JsValue): JsValue =
      JsObject(Seq("errorMessage" -> JsString(msg), "errorObject" -> obj))
  }
}