name := """wsh_api"""
organization := "com.orange"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3"
)

libraryDependencies ++= Seq(
  guice,
  "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.1" % Test,
   ws,
  "com.typesafe" % "config" % "1.3.3",
  "org.elasticsearch.client" % "elasticsearch-rest-high-level-client" % "7.0.1",
  "org.apache.tika" % "tika-core" % "1.23",
  "org.apache.tika" % "tika-parsers" % "1.23"
).map(_.exclude("org.slf4j", "*"))


