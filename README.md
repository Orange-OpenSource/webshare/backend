# WebShare

This project intends to allow a user to share Webpages with a community, and allow a community to search into Webpages shared by all users.

## Operating mode - from firefox addon* (* Coming soon!)

The addon has 2 main options: indexing or searching

### Indexing a Web Page

From a Web Page one is currently visiting (right click in the page or click on addon icon), the addon can be activated to index this webpage in the server. 

Along with indexing, tag(s) can be associated with the Webpage. 
Optionally, it's also possible to associate a quote from the Webpage (select a portion of text before addon activation).

### Searching

All Web Pages indexed by different users are all searchable from a dedicated GUI. 
There, one can perform plain text search and can filter results using Tags.

Tags may be as well expressing semantic information about the page (as a category), 
or designing a particular group of interest or any Community with who the user want to share the page. 
From the Search UI, one can navigate through the existing tags. 
From the plugin one can also retrieve any tag and set it as its favorite and use it to index afterwards.

## Architecture

The tool here consists of 2 parts, a REST server and a Firefox plugin allowing to connect this server. 

The server part consists of 4 docker containers, interacting as described in the following schema:


![WebShare-Archi](./WebShare-Archi.png)  

### Docker components detail

* The backend REST API allow the following operations:
 - create elacsticsearch indices if they don't exist
 - send a webpage for indexation in ES
 - create a tag
 - update a tag (description)
 - search for a tags
 - get a list of tags

* The Flask component serves the Newpaper3k service. This service is called to provide meaningful extraction of an Html page. It provides a text extraction where most of the noise had been cut off, and also various fields (like the published date, the authors, etc.) that are indexed (when they're available) with the text extraction itself.

* ES is where the documents are indexed and searchable, and Kibana is use here for administration purposes

## Build & Run

#### Prerequisites

You need to have `docker` and `docker-compose` installed in the environment you wish to run WebShare.  

You also need to have an Internet access.   

##### Proxy

If this access is behind a proxy, refers to the pretty good articles [here](https://medium.com/@airman604/getting-docker-to-work-with-a-proxy-server-fadec841194e) or [here](https://medium.com/@bennyh/docker-and-proxy-88148a3f35f7) to set your docker environment.

Also, if you operate inside a VM (or experienced DNS problem), you may need to specify your proxy as an IP in the Flask Dockerfile (see commented `ENV` lines inside).  
This would be if you get a warning/error mentioning a `"failure in name resolution"`. 


#### [Build &] Run containers

The server is built (if necessary) & run all at once using the docker-compose file available under the WebShare root directory.
This will download all dependencies & start all server's containers.  

`> docker-compose up -d`

To start the API part in development mode, one would comment the `playapi` section in `docker-compose.yml`, go to Backend directory, and call the runDev.sh script.

#### Install the plugin

Once all containers are started, install the plugin in Firefox, set the server endpoint, and start using it.  
**/!\** At this point, the plugin is not pushed yet. When it is, refer to its own doc for all details.


## License

@Copyright 2020 Orange

This software is distributed under the terms of the MIT license, please see license file.
Images are distributed under the terms of the CC BY 4.0 creative commons license, please see license file.

## Authors

Muriel Le Metayer

